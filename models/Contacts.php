<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property int $region_id
 * @property string $phones
 * @property string $address
 * @property string $lat
 * @property string $lng
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Region $region
 * @property ContactsFeedback[] $contactsFeedbacks
 */
class Contacts extends \yii\db\ActiveRecord
{
    public $all_phones;
    public $phone;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {
        $this->phones = Json::encode($this->phone);  // All phones change to Json and save to 'phones';

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $phones = Json::decode($this->phones);
        if(!empty($phones)){
            $k = 0;
            foreach ($phones as $item){
                $this->all_phones[$k]['phone'] = $item;
                $k++;
            }
        }

        parent::afterFind();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_id', 'address'], 'required'],
            [['region_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['phones', 'address', 'lat', 'lng'], 'string', 'max' => 255],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
            [['all_phones','phone','email'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'phones' => Yii::t('app', 'Phones'),
            'address' => Yii::t('app', 'Address'),
            'email' => Yii::t('app', 'Email'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'all_phones' => Yii::t('app', 'Phones'),
            'region.name_ru' => Yii::t('app', 'Region'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsFeedbacks()
    {
        return $this->hasMany(ContactsFeedback::className(), ['contact_id' => 'id']);
    }
}
