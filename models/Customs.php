<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customs".
 *
 * @property int $id
 * @property string $path
 * @property string $name
 * @property string $site
 *
 * @property DeliveryForm[] $deliveryForms
 */
class Customs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customs';
    }
    public function behaviors()
    {
        return [
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'name', 'site'], 'required'],
            [['name', 'site'], 'string', 'max' => 255],
            [['path'], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'Image'),
            'name' => Yii::t('app', 'Name'),
            'site' => Yii::t('app', 'Site'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryForms()
    {
        return $this->hasMany(DeliveryForm::className(), ['customs_id' => 'id']);
    }
}
