<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "rates".
 *
 * @property int $id
 * @property string $path
 * @property string $title_ru
 * @property string $title_uz
 * @property string $content_ru
 * @property string $content_uz
 * @property int $created_at
 * @property int $updated_at
 */
class Rates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rates';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_uz', 'content_ru', 'content_uz'], 'required'],
            [['content_ru', 'content_uz'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['title_ru', 'title_uz'], 'string', 'max' => 255],
            [['path'], 'file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'File for download'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_uz' => Yii::t('app', 'Content Uz'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
