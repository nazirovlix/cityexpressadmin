<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeliveryForm;

/**
 * DeliveryFormSearch represents the model behind the search form of `app\models\DeliveryForm`.
 */
class DeliveryFormSearch extends DeliveryForm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customs_id', 'cargo_kg', 'cargo_volume', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'phone', 'from_address', 'from_whereabout', 'from_date', 'from_time', 'from_comment', 'cargo_info', 'cargo_img'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeliveryForm::find()->orderBy(['status' => SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customs_id' => $this->customs_id,
            'cargo_kg' => $this->cargo_kg,
            'cargo_volume' => $this->cargo_volume,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'from_address', $this->from_address])
            ->andFilterWhere(['like', 'from_whereabout', $this->from_whereabout])
            ->andFilterWhere(['like', 'from_date', $this->from_date])
            ->andFilterWhere(['like', 'from_time', $this->from_time])
            ->andFilterWhere(['like', 'from_comment', $this->from_comment])
            ->andFilterWhere(['like', 'cargo_info', $this->cargo_info])
            ->andFilterWhere(['like', 'cargo_img', $this->cargo_img]);

        return $dataProvider;
    }
}
