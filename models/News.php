<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $category_id
 * @property string $path
 * @property string $author
 * @property string $title_ru
 * @property string $title_uz
 * @property string $content_ru
 * @property string $content_uz
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property NewsCategory $category
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'author', 'title_ru', 'title_uz', 'content_ru', 'content_uz'], 'required'],
            [['category_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['content_ru', 'content_uz'], 'string'],
            [['author', 'title_ru', 'title_uz'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewsCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['path'], 'file']  // Upload file(image)
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'path' => Yii::t('app', 'Image'),
            'author' => Yii::t('app', 'Author'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_uz' => Yii::t('app', 'Content Uz'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'category.title_ru' => Yii::t('app', 'Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['id' => 'category_id']);
    }
}
