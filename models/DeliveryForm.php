<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;

/**
 * This is the model class for table "delivery_form".
 *
 * @property int $id
 * @property int $customs_id
 * @property string $name
 * @property string $phone
 * @property string $from_address
 * @property string $from_whereabout
 * @property string $from_date
 * @property string $from_time
 * @property string $from_comment
 * @property string $cargo_info
 * @property string $cargo_img
 * @property int $cargo_kg
 * @property int $cargo_volume
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Customs $customs
 */
class DeliveryForm extends \yii\db\ActiveRecord
{
    public $delivery_to;
    public $to_address;
    public $to_whereabout;
    public $to_date;
    public $to_time;
    public $to_comment;

    public $file;       // upload cargo_img

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_form';
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'cargo_img',
                //'baseUrlAttribute' => 'base_url',
                'baseUrlAttribute' => false,
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ( is_array($this->delivery_to) ) {
            if (!$this->isNewRecord)
                DeliveryToAddress::deleteAll(['delivery_form_id' => $this->id]);
            foreach ( $this->delivery_to as $param ) {
                try {
                    Yii::$app->db->createCommand()->insert('delivery_to_address', [
                        'delivery_form_id' => $this->id,
                        'to_address' => $param['to_address'],
                        'to_whereabout' => $param['to_whereabout'],
                        'to_date' => $param['to_date'],
                        'to_time' => $param['to_time'],
                        'to_comment' => $param['to_comment'],
                    ])->execute();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {
        $params = DeliveryToAddress::find()->where(['delivery_form_id' => $this->id])->all();
        $k = 0;
        foreach ($params as $item):
            $this->delivery_to[$k]['to_address'] = $item->to_address;
            $this->delivery_to[$k]['to_whereabout'] = $item->to_whereabout;
            $this->delivery_to[$k]['to_date'] = $item->to_date;
            $this->delivery_to[$k]['to_time'] = $item->to_time;
            $this->delivery_to[$k]['to_comment'] = $item->to_comment;
            $k ++;
        endforeach;

        parent::afterFind();
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customs_id', 'cargo_kg', 'cargo_volume', 'status', 'created_at', 'updated_at'], 'integer'],
            [['from_whereabout', 'from_comment', 'cargo_info'], 'string'],
            [['name', 'phone', 'from_address', 'from_date', 'from_time', 'cargo_img'], 'string', 'max' => 255],
            [['customs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customs::className(), 'targetAttribute' => ['customs_id' => 'id']],
            [['delivery_to','to_address','to_whereabout','to_date','to_time','to_comment','file'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Delivery №'),
            'customs_id' => Yii::t('app', 'Customs ID'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'from_address' => Yii::t('app', 'From Address'),
            'from_whereabout' => Yii::t('app', 'From Whereabout'),
            'from_date' => Yii::t('app', 'From Date'),
            'from_time' => Yii::t('app', 'From Time'),
            'from_comment' => Yii::t('app', 'From Comment'),
            'cargo_info' => Yii::t('app', 'Cargo Info'),
            'cargo_img' => Yii::t('app', 'Cargo Img'),
            'cargo_kg' => Yii::t('app', 'Cargo Kg'),
            'cargo_volume' => Yii::t('app', 'Cargo Volume'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'customs.name' => Yii::t('app', 'Custom Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustoms()
    {
        return $this->hasOne(Customs::className(), ['id' => 'customs_id']);
    }

    public function getAddressTo()
    {
        return $this->hasMany(DeliveryToAddress::className(), ['delivery_form_id' => 'id']);
    }
}
