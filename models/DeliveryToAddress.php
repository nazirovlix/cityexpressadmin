<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delivery_to_address".
 *
 * @property int $id
 * @property int $delivery_form_id
 * @property string $to_address
 * @property string $to_whereabout
 * @property string $to_date
 * @property string $to_time
 * @property string $to_comment
 */
class DeliveryToAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_to_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['delivery_form_id', 'to_address', 'to_date', 'to_time'], 'required'],
            [['delivery_form_id'], 'integer'],
            [['to_whereabout', 'to_comment'], 'string'],
            [['to_address', 'to_date', 'to_time'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Delivery №'),
            'delivery_form_id' => Yii::t('app', 'Delivery Form ID'),
            'to_address' => Yii::t('app', 'To Address'),
            'to_whereabout' => Yii::t('app', 'To Whereabout'),
            'to_date' => Yii::t('app', 'To Date'),
            'to_time' => Yii::t('app', 'To Time'),
            'to_comment' => Yii::t('app', 'To Comment'),
        ];
    }
}
