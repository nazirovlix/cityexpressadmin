<?php

use yii\db\Migration;

/**
 * Handles the creation of table `application`.
 */
class m180627_111008_create_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('application', [
            'id' => $this->primaryKey(),
            'company' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'phone' => $this->string(255),
            'massege' => $this->text()->notNull(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(255),
            'status' => $this->integer(6)->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('application');
    }
}
