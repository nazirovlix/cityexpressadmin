<?php
/**
 * Created by PhpStorm.
 * User: MrLIX
 * Date: 27.06.2018
 * Time: 17:12
 */


//// ================ FRONT_END =====================

// Carousel

// Carousel::find()->where(['page' => 'home'])->all();
//      [
//            'home',       => for Home page
//            'about',      => for About
//            'tarif1',     => for Tarif 1 page
//            'tarif2',     => for Tarif 2 page
//            'tarif3',     => for Tarif 3 page
//            'service1',   => for Service 1 page
//            'service2',   => for Service 1 page
//            'service3',   => for Service 1 page
//            'news',       => for News
//            'contacts',   => for Contacts
//      ],
//
//=======




///////////////////////// ADMIN ///////////////////////////////
// ======== Image Path =========
// Behaviors
    public function behaviors()
    {
        return [
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],

        ];
    }

// Form view ?>

  <?= $form->field($model, 'path')->fileInput(['class' => 'dropify', 'data-default-file' => '/uploads/' . $model->path]) ?>

<?php

// For View or Index
    [
        'attribute' => 'path',
        'value' => function($model){
            return '<img src="/uploads/'.$model->path.'" width="150px">';
        },
        'format' => 'raw',

    ],
//------------------------

// Status
    [
        'attribute' => 'status',
        'value' => function($model){
            if($model->status == 1):
                return '<span style="color: #0b58a2;">Published</span>';
            else:
                return '<span style="color: #ff0c3e">Not published</span>';

            endif;

        },
        'format' => 'html',
        'filter' => [
            '1' => 'Pulished',
            '0' => 'Not published'
        ],
    ],

