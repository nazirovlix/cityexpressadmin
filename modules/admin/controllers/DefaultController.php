<?php

namespace app\modules\admin\controllers;

use app\models\Customs;
use app\models\DeliveryForm;
use app\models\Feedback;
use app\models\Reviews;
use mdm\admin\models\form\Login;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $delivery = DeliveryForm::find()->orderBy(['status' => 0])->limit(10)->all();
        $review = Reviews::find()->limit(4)->all();
        $del_count = DeliveryForm::find()->count();
        $cus_count = Customs::find()->count();
        $feed_count = Feedback::find()->count();
        $last = DeliveryForm::find()->orderBy(['id' => SORT_DESC])->one();
        $reviews = Reviews::find()->orderBy(['id' => SORT_DESC])->limit(4)->all();
        $feedback = Feedback::find()->orderBy(['id' => SORT_DESC])->limit(3)->all();
        return $this->render('index', compact([
            'delivery',
            'review',
            'del_count',
            'cus_count',
            'feed_count',
            'last',
            'reviews',
            'feedback'
        ]));
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new Login();

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
}
