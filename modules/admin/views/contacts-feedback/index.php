<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ContactsFeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts Feedbacks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Contacts Feedback'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                    'attribute' => 'contact_id',
                    'value' => function($model){
                        return '<a href="'. Url::to(['contacts/view','id' => $model->contact_id]).'">Contacts</a>';
                    },
                    'format' => 'html'
            ],
            'name',
            'phone',
            'massege:ntext',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 1):
                        return '<span style="color: #0b58a2;">Read</span>';
                    else:
                        return '<span style="color: #ff0c3e">Not read</span>';

                    endif;

                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Read',
                    '0' => 'Not read'
                ],
            ],
            'created_at:date',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
