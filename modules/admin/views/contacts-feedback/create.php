<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContactsFeedback */

$this->title = Yii::t('app', 'Create Contacts Feedback');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contacts Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-feedback-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
