<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CarouselSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Carousels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Carousel'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'path',
                'value' => function($model){
                    return '<img src="/uploads/'.$model->path.'" width="150px">';
                },
                'format' => 'raw',

            ],
            'url:url',
            'title_ru',
            //'title_uz',
            //'content_ru:ntext',
            //'content_uz:ntext',
            [
                    'attribute' => 'page',
                    'filter' => [
                        'home' => 'home',
                        'about' => 'about',
                        'tarif1' => 'tarif 1',
                        'tarif2' => 'tarif 2',
                        'tarif3' => 'tarif 3',
                        'service1' => 'service 1',
                        'service2' => 'service 2',
                        'service3' => 'service 3',
                        'news' => 'news',
                        'contacts' => 'contacts'
                    ],
            ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
