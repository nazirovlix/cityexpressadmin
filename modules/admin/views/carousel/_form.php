<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Carousel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'path')->fileInput(['class' => 'dropify', 'data-default-file' => '/uploads/' . $model->path]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'placeholder' => 'http://site.com']) ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content_ru')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content_uz')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'page')->dropDownList([
            'home' => 'home',
            'about' => 'about',
            'tarif1' => 'tarif 1',
            'tarif2' => 'tarif 2',
            'tarif3' => 'tarif 3',
            'service1' => 'service 1',
            'service2' => 'service 2',
            'service3' => 'service 3',
            'news' => 'news',
            'contacts' => 'contacts'
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
