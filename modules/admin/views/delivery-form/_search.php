<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\DeliveryFormSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'customs_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'phone') ?>

    <?= $form->field($model, 'from_address') ?>

    <?php // echo $form->field($model, 'from_whereabout') ?>

    <?php // echo $form->field($model, 'from_date') ?>

    <?php // echo $form->field($model, 'from_time') ?>

    <?php // echo $form->field($model, 'from_comment') ?>

    <?php // echo $form->field($model, 'cargo_info') ?>

    <?php // echo $form->field($model, 'cargo_img') ?>

    <?php // echo $form->field($model, 'cargo_kg') ?>

    <?php // echo $form->field($model, 'cargo_volume') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
