<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\DeliveryFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Delivery Forms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-form-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Delivery Form'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'customs.name',
            [
                    'attribute' => 'id',
                    'value' => function($model){
                        return '<a href="'.\yii\helpers\Url::to(['delivery-form/view', 'id' => $model->id]).'">000'.$model->id.'</a>';
                    },
                    'format' => 'html'
            ],

            'name',
            'phone',
            'from_address',
            //'from_whereabout:ntext',
            //'from_date',
            //'from_time',
            //'from_comment:ntext',
            //'cargo_info:ntext',
            //'cargo_img',
            //'cargo_kg',
            //'cargo_volume',
            'created_at:date',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 1):
                        return '<span style="color: #0b58a2;">Done</span>';
                    else:
                        return '<span style="color: #ff0c3e">Waiting</span>';

                    endif;

                },
                'format' => 'html',
                'filter' => [
                    '1' => 'Done',
                    '0' => 'Waiting'
                ],
            ],

            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
