<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DeliveryForm */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Delivery Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-form-view">



    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <div class="container">
        <h1>Delivery № <?= $model->id ?></h1>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Custom</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><a href="<?= Url::to(['customs/view', 'id' => $model->customs->id]) ?>"><?= $model->customs->name ?></a></td>
                <td><?= $model->name ?></td>
                <td><?= $model->phone ?></td>

                <td><?= date('d.m.Y H:m', $model->created_at) ?></td>
                <td><?= date('d.m.Y H:m', $model->updated_at) ?></td>
                <td><?php
                    if($model->status == 1):
                        echo '<span style="color: #0b58a2;">Done</span>';
                    else:
                        echo '<span style="color: #ff0c3e">Waiting</span>';

                    endif;
                    ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<br>
    <div class="container">
        <h1>Address From</h1>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Address</th>
                <th>Whereabout</th>
                <th>Date</th>
                <th>Time</th>
                <th>Comment</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?= $model->from_address ?></td>
                <td><?= $model->from_whereabout ?></td>
                <td><?= $model->from_date ?></td>
                <td><?= $model->from_time ?></td>
                <td><?= $model->from_comment ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>
    <div class="container">
        <h1>Address To</h1>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>№</th>
                <th>Address</th>
                <th>Whereabout</th>
                <th>Date</th>
                <th>Time</th>
                <th>Comment</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($model->addressTo)): ?>
                <?php foreach ($model->addressTo as $k => $item): ?>
                <tr>
                    <td><?= $k + 1 ?></td>
                    <td><?= $item->to_address ?></td>
                    <td><?= $item->to_whereabout ?></td>
                    <td><?= $item->to_date ?></td>
                    <td><?= $item->to_time ?></td>
                    <td><?= $item->to_comment ?></td>
                </tr>
                    <?php endforeach ;?>
            <?php endif;?>
            </tbody>
        </table>
    </div>
    <br>
    <div class="container">
        <h1>About Cargo</h1>

        <table class="table table-bordered">
            <thead>
            <tr>

                <th>Image</th>
                <th>Information</th>
                <th>Weight (kg)</th>
                <th>Volume (kube/m)</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <a href="/uploads/<?= $model->cargo_img ?>" target="_blank">
                        <img src="/uploads/<?= $model->cargo_img ?>" width="100px">
                    </a>
                </td>
                <td><?= $model->cargo_info ?></td>
                <td><?= $model->cargo_kg ?> kg</td>
                <td><?= $model->cargo_volume ?></td>

            </tr>
            </tbody>
        </table>
    </div>

</div>

