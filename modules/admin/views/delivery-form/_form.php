<?php

use app\models\Customs;
use trntv\filekit\widget\Upload;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DeliveryForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customs_id')->dropDownList(ArrayHelper::map(Customs::find()->all(), 'id', 'name')) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'from_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'from_whereabout')->textarea(['rows' => 6]) ?>
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'from_date')->widget(
                trntv\yii\datetime\DateTimeWidget::className(),
                [
                    'phpDatetimeFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'minDate' => new \yii\web\JsExpression('new Date()'),
                        'allowInputToggle' => false,
                        'showTodayButton' => true,
                        'showClose' => true,
                        'sideBySide' => true,
                        'locale' => 'ru',
                        'widgetPositioning' => [
                            'horizontal' => 'auto',
                            'vertical' => 'auto'
                        ]
                    ]

                ]
            )->label('From Date'); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'from_time')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?= $form->field($model, 'from_comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'delivery_to')->widget(MultipleInput::className(), [
        'rowOptions' => [

        ],
        'columns' => [
            [
                'name' => 'to_address',
                'options' => [
                    'placeholder' => 'Address to'
                ]
            ],
            [
                'name' => 'to_whereabout',
                'options' => [
                    'placeholder' => 'Whereabout to'
                ]
            ],
            [
                'name' => 'to_date',
                'options' => [
                    'placeholder' => 'Date'
                ]
            ],
            [
                'name' => 'to_time',
                'options' => [
                    'placeholder' => 'Time'
                ]
            ],
            [
                'name' => 'to_comment',
                'options' => [
                    'placeholder' => 'Comment'
                ]
            ]

        ],
        'addButtonOptions' => ['class' => 'btn btn-success']
    ])->label('Delivery Address to') ?>

    <?= $form->field($model, 'cargo_info')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'file')->widget(
                Upload::className(),
                [
                    'url' => ['/admin/file-storage/upload'],
                    'sortable' => true,
                    'maxFileSize' => 10 * 1024 * 1024, // 10 MiB

                ])->label('Cargo Image');
            ?>
        </div>
        <div class="col-md-9">

            <?= $form->field($model, 'cargo_kg')->textInput() ?>

            <?= $form->field($model, 'cargo_volume')->textInput() ?>
        </div>
    </div>


    <?= $form->field($model, 'status')->checkbox() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
