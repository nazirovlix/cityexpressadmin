<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SocialsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Socials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="socials-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Socials'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'path',
                'value' => function($model){
                    return '<img src="/uploads/'.$model->path.'" width="50px">';
                },
                'format' => 'raw',

            ],
            [
                    'attribute' => 'name',
                    'filter' => [
                        'facebook' => 'facebook',
                        'instagram' => 'instagram',
                        'twitter' => 'twitter',
                        'telegram' => 'telegram',
                        'youtube' => 'youtube',
                        'appstore' => 'AppStore',
                        'googleplay' => 'GooglePlay'
                    ]
            ],
            'url:url',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 1):
                        return '<span style="color: #0b58a2;">Published</span>';
                    else:
                        return '<span style="color: #ff0c3e">Not published</span>';

                    endif;

                },
                'format' => 'html',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
