<?php
/* @var $this \yii\web\View */

/* @var $content string */

use app\models\Application;
use app\models\ContactsFeedback;
use app\models\Feedback;
use app\models\Subscribe;
use app\modules\admin\assets\AdminAsset;
use app\modules\admin\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Admin Panel application';

$apps = Application::find()->where(['status' => 0])->all();
$subs = Subscribe::find()->where(['status' => 0])->all();
$feedback = Feedback::find()->where(['status' => 0])->all();


AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
        </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">
                        <!-- Logo icon -->

                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span><img src="/images/u2.jpg" alt="homepage" class="dark-logo" width="190px"/></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item">
                            <a class="nav-link nav-toggler hidden-md-up text-muted" href="javascript:void(0)">
                                <i class="mdi mdi-menu"></i>
                            </a>
                        </li>
                        <li class="nav-item m-l-10">
                            <a class="nav-link sidebartoggler hidden-sm-down text-muted" href="javascript:void(0)">
                                <i class="ti-menu"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                    <?php if(!empty($apps)): ?>
                        <!-- Comment -->
                        <li class="nav-item dropdown">

                            <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-flag"></i>
                                <div class="notify">
                                    <span class="heartbit"></span>
                                    <span class="point"></span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                <ul>
                                    <li>
                                        <div class="drop-title">Applications (<?= count($apps) ?>)</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <?php foreach ($apps as $item): ?>
                                            <!-- Message -->
                                            <a href="<?= Url::to(['application/view', 'id' => $item->id]) ?>">

                                                <div class="mail-contnet">
                                                    <h5><?= $item->name ?></h5>
                                                    <span style="font-size: 12px">Company: <?= $item->company ?></span>
                                                    <span class="mail-desc"><?= mb_substr($item->massege, 0, 60)?>..</span>
                                                    <span class="time"><?= date( 'd.m.Y',$item->created_at) ?></span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <?php endforeach; ?>

                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="<?= Url::to(['application/index']) ?>">
                                            <strong>All Aplications</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- End Comment -->
                    <?php endif; ?>

                    <?php if(!empty($subs)): ?>
                            <!-- Subscribes -->
                            <li class="nav-item dropdown">

                                <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-share-alt"></i>
                                    <div class="notify">
                                        <span class="heartbit"></span>
                                        <span class="point"></span>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                    <ul>
                                        <li>
                                            <div class="drop-title">Subscribes (<?= count($subs) ?>)</div>
                                        </li>
                                        <li>
                                            <div class="message-center">
                                                <?php foreach ($subs as $item): ?>
                                                    <!-- Message -->
                                                    <a href="<?= Url::to(['subscribe/view', 'id' => $item->id]) ?>">

                                                        <div class="mail-contnet">
                                                            <h5><?= $item->name ?></h5>
                                                            <span class="mail-desc"><?= $item->email ?></span>
                                                            <span class="time"><?= date( 'd.m.Y',$item->created_at) ?></span>
                                                        </div>
                                                    </a>
                                                    <!-- Message -->
                                                <?php endforeach; ?>

                                            </div>
                                        </li>
                                        <li>
                                            <a class="nav-link text-center" href="<?= Url::to(['subscribe/index']) ?>">
                                                <strong>All Subscribes</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- End Comment -->
                        <?php endif; ?>

                    <?php if(!empty($feedback)): ?>
                        <!-- Messages -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" id="2" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-envelope"></i>
                                <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" aria-labelledby="2">
                                <ul>
                                    <li>
                                        <div class="drop-title">You have <?= count($feedback) ?> new feedback</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <?php foreach ($feedback as $item): ?>
                                            <!-- Message -->
                                            <a href="<?= Url::to(['feedback/view', 'id' => $item->id]) ?>">

                                                <div class="mail-contnet">
                                                    <h5><?= $item->name ?></h5>
                                                    <span class="mail-desc"><?= mb_substr($item->massege,0,80)?>...</span>
                                                    <span class="time"><?= date('d.m.Y h:i',$item->created_at) ?></span>
                                                </div>
                                            </a>
                                            <?php endforeach; ?>

                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="<?= Url::to(['feedback/index'])?>">
                                            <strong>See all feedback</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- End Messages -->
                    <?php endif; ?>
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted" href="#" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <img src="/images/users/5.jpg" alt="user" class="profile-pic"/>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="#"><i class="ti-user"></i> Profile</a></li>
                                    <li><a href="#"><i class="ti-wallet"></i> Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li><a href="#"><i class="ti-settings"></i> Setting</a></li>
                                    <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?= $this->render('sidebar.php') ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <?php if ( Yii::$app->controller->id !== 'default' ): ?>
                        <?= Html::a('Create ', [ 'create' ], [ 'class' => 'btn btn-primary' ]) ?>
                    <?php else: ?>

                    <?php endif; ?>
                </div>
                <div class="col-md-7 align-self-center">
                    <?php try {
                        echo Breadcrumbs::widget([
                            'tag' => 'ol',
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]);
                    } catch ( Exception $e ) {
                        echo $e->getMessage();
                    } ?>
                </div>
            </div>
            <div class="container-fluid">
                <!-- Start Page Content -->
                <?php if ( Yii::$app->controller->id !== 'default' ): ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card" id="admin-content">
                                <?= $content; ?>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <?= $content; ?>
                <?php endif; ?>
            </div>
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. Template designed by <a href="https://colorlib.com">Colorlib</a>
            </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>