<?php
/**
 * Created by PhpStorm.
 * User: Farhodjon
 * Date: 10.03.2018
 * Time: 15:17
 */

use app\models\Application;
use app\models\ContactsFeedback;
use app\models\DeliveryForm;
use app\models\Feedback;
use app\models\Subscribe;
use app\modules\admin\widgets\Menu;
use yii\helpers\Url;
$cont = ContactsFeedback::find()->where(['status' => 0])->count();
$feedback = Feedback::find()->where(['status' => 0])->count();
$apps = Application::find()->where(['status' => 0])->count();
$subs = Subscribe::find()->where(['status' => 0])->count();
$delivery = DeliveryForm::find()->where(['status' => 0])->count();
?>
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <?php
            try {
                echo Menu::widget([
                    'options' => [ 'id' => 'sidebarnav' ],
                    'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                    'badgeClass' => 'label label-rouded label-primary pull-right',
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => '',
                            'options' => [ 'class' => 'nav-devider' ]
                        ],
                        [
                            'label' => 'Home',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Dashboard',
                            'url' => ['default/index'],
                            'icon' => '<i class="fa fa-tachometer"></i>',
                        ],
                        [
                            'label' => 'App',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Home Page',
                            'url' => '#',
                            'icon' => '<i class="fa fa-home"></i>',
                            'items' => [
                                [
                                    'label' => 'Carousel',
                                    'url' => ['carousel/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Partners',
                                    'url' => ['partners/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Social networks',
                                    'url' => ['socials/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Reviews',
                                    'url' => ['reviews/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                            ]
                        ],
                        [
                            'label' => 'Pages',
                            'url' => '#',
                            'icon' => '<i class="fa fa-file-text-o"></i>',
                            'items' => [
                                [
                                    'label' => 'About',
                                    'url' => ['about/view', 'id' => 1],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Rates',
                                    'url' => ['rates/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Services',
                                    'url' => ['services/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                            ]
                        ],
                        [
                            'label' => 'News',
                            'url' => '#',
                            'icon' => '<i class="fa fa-newspaper-o"></i>',
                            'items' => [

                                [
                                    'label' => 'News Category',
                                    'url' => ['news-category/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'News',
                                    'url' => ['news/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Subscribes',
                                    'badge' => $subs,
                                    'url' => ['subscribe/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                            ]
                        ],
                        [
                            'label' => 'Feedback',
                            'url' => '#',
                            'icon' => '<i class="fa fa-envelope-o"></i>',
                            'items' => [
                                [
                                    'label' => 'Feedback',
                                    'badge' => $feedback,
                                    'url' => ['feedback/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Application',
                                    'badge' => $apps,
                                    'url' => ['application/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                            ]
                        ],
                        [
                            'label' => 'Contacts',
                            //'badge' => $cont,
                            'url' => '#',
                            'icon' => '<i class="fa fa-phone-square"></i>',
                            'items' => [
                                [
                                    'label' => 'Main Contact',
                                    'url' => ['contacts/view', 'id' => 1],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Contacts',
                                    'url' => ['contacts/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Region',
                                    'url' => ['region/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Contacts Feedback',
                                    'badge' => $cont,
                                    'url' => ['contacts-feedback/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                            ]
                        ],
                        [
                            'label' => 'Delivery',
                            'url' => '#',
                            'badge' => $delivery,
                            'icon' => '<i class="fa fa-truck"></i>',
                            'items' => [
                                [
                                    'label' => 'Deliveries',
                                    //'badge' => $delivery,
                                    'url' => ['delivery-form/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                                [
                                    'label' => 'Customs',
                                    'url' => ['customs/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],


                            ]
                        ],

                ]
                ]);
            } catch ( Exception $e ) {
            }
            
            ?>
        </nav>
    </div>
</div>
