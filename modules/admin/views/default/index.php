<?php

use yii\helpers\Url;
?>

<!-- Start Page Content -->
<div class="row">
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-truck f-s-40 color-primary"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?=  $del_count ?></h2>
                    <p class="m-b-0">Total Delivery orders</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-sort-numeric-desc f-s-40 color-success"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2>000<?= $last->id ?></h2>
                    <p class="m-b-0">Last Delivery Number</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-envelope f-s-40 color-warning"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $feed_count ?></h2>
                    <p class="m-b-0">Feedback</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $cus_count ?></h2>
                    <p class="m-b-0">Customer</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4>Last Delivery Orders </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Delivery №</th>
                            <th>Photo</th>
                            <th>Customer</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Created at</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                    <?php if(!empty($delivery)): ?>
                        <?php foreach ($delivery as $item): ?>
                        <tr>
                            <td> <a href="<?= Url::to(['delivery-form/view', 'id' => $item->id])?>">000<?= $item->id ?></a></td>
                            <td>
                                <div class="round-img">
                                   <img src="/uploads/<?= $item->cargo_img ?>" alt="">
                                </div>
                            </td>
                            <td><?= $item->customs->name ?></td>
                            <td><span><?= $item->name ?></span></td>
                            <td><span><?= $item->phone ?></span></td>
                            <td><span><?= date('d.m.Y H:m', $item->created_at)?></span></td>

                            <td><?php if($item->status == 1): ?>
                                <span class="badge badge-success">Done</span>
                                <?php else: ?>
                                <span class="badge badge-warning">Waiting</span>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                   <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-8">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Feedback </h4>
                    </div>
                    <div class="recent-comment">
                        <?php if(!empty($feedback)): ?>
                            <?php foreach ($feedback as $item): ?>
                            <div class="media">

                                <div class="media-body">
                                    <h4 class="media-heading"><?= $item->name ?></h4>
                                    <p><?= mb_substr($item->massege,0,150)?>..</p>
                                    <p class="comment-date"><?= date('d.m.Y H:m',$item->created_at) ?></p>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- /# card -->
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-dark">
            <div class="testimonial-widget-one p-17">
                <div class="testimonial-widget-one owl-carousel owl-theme">
                    <?php if(!empty($reviews)): ?>
                        <?php foreach ($reviews as $item): ?>
                     <div class="item">
                        <div class="testimonial-content">
                            <img class="testimonial-author-img" src="/uploads/<?= $item->path ?>" alt="" />
                            <div class="testimonial-author"><?= $item->name ?></div>
                            <div class="testimonial-author-position"><?= $item->email ?></div>

                            <div class="testimonial-text">
                                <i class="fa fa-quote-left"></i>  <?= mb_substr($item->review,0,150) ?>
                                <i class="fa fa-quote-right"></i>
                            </div>
                        </div>
                    </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- End PAge Content -->