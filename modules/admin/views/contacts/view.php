<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contacts */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contacts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
       <? if($model->id != 1): ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?php endif; ?>
    </p>
    <? if($model->id != 1): ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'region.name_ru',
            [
                'attribute' => 'phones',
                'value' => function($model){
                    $phones = \yii\helpers\Json::decode($model->phones);
                    $phone = '';
                    if(!empty($phones)) {
                        foreach ($phones as $item) {
                            $phone .= $item .'<br>';
                        }
                    }
                    return $phone;
                },
                'format' => 'html'
            ],
            'address',
            'email',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 1):
                        return '<span style="color: #0b58a2;">Published</span>';
                    else:
                        return '<span style="color: #ff0c3e">Not published</span>';

                    endif;

                },
                'format' => 'html',

            ],
            'created_at:date',
            'updated_at:date',
        ],
    ]) ?>
    <?php else: ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                        'attribute' => 'phones',
                        'value' => function($model){
                            $phones = \yii\helpers\Json::decode($model->phones);
                            $phone = '';
                            if(!empty($phones)) {
                                foreach ($phones as $item) {
                                    $phone .= $item .'<br>';
                                }
                            }
                            return $phone;
                        },
                        'format' => 'html'
                ],
                'email',
                'address',
                'updated_at:date',
            ],
        ]) ?>
    <?php endif; ?>

    <input id="lat" class="hidden" value="<?= $model->lat; ?>" />
    <input id="lng" class="hidden" value="<?= $model->lng; ?>" />

    <div class="field" id="map2" style="height: 200px; width: 100%; margin-top: 30px">
        <script>



            function initMap( ) {
                var lat1 = Number(document.getElementById("lat").value);
                var lng1 = Number(document.getElementById("lng").value);
                var uluru = {lat: lat1, lng: lng1};
                var map = new google.maps.Map(document.getElementById('map2'), {
                    zoom: 15,
                    center: uluru

                });
                var marker = new google.maps.Marker({

                    position: uluru,
                    map: map
                });
            }


        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
        </script>

    </div>

</div>
